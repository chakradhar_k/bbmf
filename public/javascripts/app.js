/**
 * Created by Admin on 1/7/2016.
 */

var bmfApp = angular.module('bmfApp', []);

// create the controller and inject Angular's $scope
bmfApp.controller('HomeController', function ($scope, $http) {

    // create a message to display in our view
    $scope.message = 'Everyone come and see how good I look!';


    $scope.plugins = ['plugin1', 'plugin2', 'plugin3', 'plugin4'];

    $scope.selection = [];

    $scope.info = {
        'project': {
            'name': '',
            'type': '',
            'platform':''
        }, 'framework': {
            'name': '',
            'version': ''
        }, 'plugins': $scope.selection
    };


    $scope.project = {created: false, dUrl: '#'};
    // toggle selection for a given fruit by name
    $scope.toggleSelection = function toggleSelection(pluginName) {
        var idx = $scope.selection.indexOf(pluginName);

        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selection.push(pluginName);
        }
    };

    $scope.createProject = function () {

        var req = {
            method: 'POST',
            url: '/saveFile',
            headers: {
                'Content-Type': 'application/json'
            },
            data: $scope.info
        }

        $http(req).then(function (response) {
            alert("Project is created at S3, please download project using download button");
            $scope.project.created = true;
            $scope.project.dUrl = response.data.dUrl;
        }, function () {
        });

    }

});
