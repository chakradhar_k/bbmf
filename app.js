var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var fs = require('fs');

var EasyZip = require('easy-zip').EasyZip;
var AWS = require('aws-sdk');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/saveFile',function(req, res, next) {
    console.log("save file in app.js");
     res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
 });

app.use('/', routes);
app.use('/users', users);
var resp;

app.post('/saveFile', function (req, res, next) {
    resp = res;
    console.log(req.body);
    var info = JSON.parse(req.body['info']);
    var fileName = info.project.name;
    // var fileName = "temp_" + new Date().getDate() + "_" + new Date().getUTCMilliseconds();
    fileWrite(fileName + ".json", req.body, function () {
    });

    // console.log(req.body);
    // console.log(JSON.parse(info));
    console.log(info.project.name);
    
    // res.send({'dURL':'no'});
    
    var plugInString = ''; 
    
    for(var p=0; p<info.plugins.length; p++) {
        plugInString = plugInString + 'mkdir '+ info.plugins[p].name + '\n';
    }
    
    var pStg= '';
    var ip = false;
    var inBuiltPlugin = '';
    for(var p=0; p<info.plugins.length; p++) {
        var pluginName = info.plugins[p].name;
        var url = info.plugins[p].url;
        if(pluginName.toString().match("encrypt")) {
            inBuiltPlugin = inBuiltPlugin+ 'cordova plugin add '+url+'\n';
            ip = true;
        }  else if(pluginName.toString().match("BMS_mobile_push_plugin")) {
            //https://chakradhar_k:c!1209A023@bitbucket.org/chakradhar_k/mobile-push-plugin.git
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ngit clone '+url+'\n';
            ip = true;
        }else if(pluginName.toString().match("App47Plugin_Android")){
            
             inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("App47Plugin_IOS")){
            
             inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("EncryptPlugin_IOS")){
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("EncryptPlugin_Android")){
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        }
        else if(pluginName.toString().match("Push_Notification_Plugin")){
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        }
        else if(pluginName.toString().match("Remotewipe")){
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("Brightcove")){
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        }
        else if(pluginName.toString().match("BrightcoveVideoplugin")){
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("DownloadPlugin_IOS")) {
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("RemoteWipe_IOS")) {
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("InboxPushNotification_IOS")) {
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        } else if(pluginName.toString().match("DownloadPlugin_Android")) {
            inBuiltPlugin = inBuiltPlugin+ 'cd plugins\ncordova plugin add '+url+'\n';
            ip = true;
        }
        else {
            // pStg = pStg + ' org.apache.cordova.'+ pluginName;    
        }
    }
    
    
    if(ip) {
        pStg = inBuiltPlugin;
    }
    // var batString = 'mkdir ' + fileName + '\n' + 'cd ' + fileName + '\n' +  
    // 'mkdir ' + info.project.name + '\n' + 'cd '+ info.project.name + '\n' +
    // 'mkdir JSLibrary\n' +
    // 'cd JSLibrary\n' +
    // 'mkdir ' + info.framework.name + '\n' +
    // 'cd ..\n'+
    // 'mkdir Plugin\n' + 
    // 'cd Plugin\n' +
    // plugInString +
    // 'cd ..\n'+
    // 'mkdir Framework\n' +
    // 'cd Framework\n' +
    // 'mkdir ' + info.project.platform + '\n';

    // if(info.project.platform.toString() == "Android") {
        
    //     info.project.platform = 'android';
        
    // } else if(info.project.platform.toLocaleString() == "IOS"){
        
    //     info.project.platform = 'ios';
        
    // }

    var batString = 'cordova create ' + info.project.name + '\n' +
    'cd ' + info.project.name + '\n' +
    'cordova platform add ' + info.project.platform + '\n' +
     pStg;
    // 'cordova build\n';
    // 'cordova plugin add' + pStg + '\n' +
    // 'cordova run ' + info.project.platform +'\n'

    // batString = 'mkdir ' + fileName + '\n' + 'cd ' + fileName + '\ngit clone https://chakradhar_k:c!1209A023@bitbucket.org/chakradhar_k/bmf_plugintemp.git';
    // //var batString = 'phonegap create '+info['project']['name']+'\n'+
    // //    'echo "project created\n'+
    // //    'cd '+info['project']['name']+'\n'+
    // //    'echo "go to folder"\n'
    // //'phonegap run android\n'
    // //'echo "running on android\n';
    fileWrite2(fileName + ".sh", batString, req, res);

});


// var multer = require('multer')

// var uploading = multer({
//   dest: __dirname + '/bin',
//   limits: {fileSize: 1000000, files:1},
// })

// app.post('/upload', uploading, function(req, res) {

// })

// write data to json file

app.post('/newPlugin', function (req, res, next) {
    // resp = res;
    console.log(req.body);
    
    fs.readFile('../public/data/demo.json', function(err, data) {
      
      if (err) console.log(err);
      
      console.log("data: "+data);
      
      var dataObject = JSON.parse(data);
      
      dataObject.plugins.push(req.body);
      
       fs.writeFile('../public/data/demo.json', JSON.stringify(dataObject), function (err) {
            if (err){ return console.log(err)
                res.send({"status":"failure","message":err});
            } else {
                 console.log("updated");
                res.send({"status":"success","message":"plugin gets uploaded successfully"});
            }
            
           
         });
      
    });
    
    console.log("set");
    
    // var info = JSON.parse(req.body['i);
    // return res.JSON({"status":"ok"});
    
    
});



function fileWrite(fileName, content, next) {
    console.log(content);
    var contentString = JSON.stringify(content);
    console.log(contentString);
    fs.writeFile(fileName, contentString, function (err) {
        if (err) return console.log(err);
        console.log('Created File > ' + fileName);
    });
}

function fileWrite2(fileName, content, req, res, next) {
    console.log(content);
    fs.writeFile(fileName, content, function (err, req, res) {
        if (err) return console.log(err);
        console.log('Created File > ' + fileName);

        var cmd1 = 'cd ' + __dirname.toString();
        var cmd2 = fileName.toString();
        console.log(cmd2);

        var exec = require('child_process').exec;
        var proc = exec(fileName);

        proc.stdout.setEncoding('utf8');

        proc.stdout.on('data', function (chunk) {
            console.log(chunk);
        });

        proc.stdout.on('end', function (req, res) {
            console.log('ended');
            //add folder

            var spawn = require('child_process').spawn;
            var _ = require('underscore'); // for some utility goodness
            var deploySh = spawn('sh', [fileName ], {}, function(){
                
            });
            
            deploySh.stdout.on('data', function(data) {
              console.log('stdout: '+ data);
            });
            
            deploySh.stderr.on('data', function(data) {
              console.log('stderr: '+ data);
            });
            
            deploySh.on('close', function(code, req, res) {
              console.log('child process exited with code '+ code);
              //zip a folder
              
                var folderName = fileName.replace('.sh', '');
              
                var archiver = require('archiver');

                var output = fs.createWriteStream(__dirname + "/bin/"+folderName+'.zip');
                var archive = archiver('zip');
                
                output.on('close', function(req, res) {
                  console.log(archive.pointer() + ' total bytes');
                  console.log('archiver has been finalized and the output file descriptor has closed.');
                  fileWriteToAWS(folderName + '.zip', req, res);
                });
                
                archive.on('error', function(err) {
                  throw err;
                });
                
                archive.pipe(output);
                
                // var file1 = __dirname + "/"+folderName;
                
                archive.bulk([
                  { expand: true, cwd: folderName+'/', src: ['**'] }
                ]);
                
                archive.finalize();
                
                
                // var zip5 = new EasyZip();
               
                // zip5.zipFolder(folderName, function (req, res) {
                //     zip5.writeToFile(folderName + '.zip', function (req, res) {
                //         fileWriteToAWS(folderName + '.zip', req, res);
                //     });
                // });
            });
            
        });

    });
}

function fileWriteToAWS(fileName, req, res) {
    console.log("file upload to aws");
    AWS.config.loadFromPath('../config.json');
    s3Stream = require('s3-upload-stream')(new AWS.S3());

    var read = fs.createReadStream(fileName);

    var upload = s3Stream.upload({
        Bucket: "fitrospayu",
        Key: fileName,
        ACL: "public-read",
        StorageClass: "REDUCED_REDUNDANCY",
        ContentType: "zip"
    });

    upload.maxPartSize(20971520); // 20 MB
    upload.concurrentParts(5);

    upload.on('error', function (error) {
        console.log("Error: " + error);
    });

    upload.on('part', function (details) {
        console.log("part" + details);
    });

    upload.on('uploaded', function (details, req, res) {
        console.log("uploaded: " + JSON.stringify(details));
        obj = details;
        s3UploadCallBack(obj.Location);
        //res.send({'dURL': obj.Location});
    });

    read.pipe(upload);

}

var s3UploadCallBack = function (url) {
    resp.json({dUrl: url});
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
